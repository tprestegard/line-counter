package cmd

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path/filepath"
)

func countFileLines(filename string, counter LineCounter) int {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	lineCount := 0
	for scanner.Scan() {
		if counter.CountLine(scanner.Text()) {
			//fmt.Println(scanner.Text())
			lineCount++
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return lineCount
}

func detectFileType(filename string) (FileType, error) {
	ext := filepath.Ext(filename)
	if ext == ".go" {
		return GolangFileType, nil
	} else if ext == ".py" {
		return PythonFileType, nil
	}
	return FileType(""), fmt.Errorf("invalid filetype detected with extension '%s'", ext)
}
