package cmd

import (
	"fmt"
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDetectFileType(t *testing.T) {
	cases := []struct {
		filename         string
		expectedFileType FileType
	}{
		{
			filename:         "/opt/path/file.py",
			expectedFileType: PythonFileType,
		},
		{
			filename:         "file1234-abcd83.py",
			expectedFileType: PythonFileType,
		},
		{
			filename:         "/opt/path with space/file.go",
			expectedFileType: GolangFileType,
		},
		{
			filename:         "./localfile-test.go",
			expectedFileType: GolangFileType,
		},
	}

	for _, c := range cases {
		fileType, err := detectFileType(c.filename)
		assert.Equal(t, c.expectedFileType, fileType)
		assert.NoError(t, err)
	}

}

func TestDetectFileTypeInvalidType(t *testing.T) {
	ext := ".badext"
	fileType, err := detectFileType("file" + ext)
	assert.Equal(t, FileType(""), fileType)
	assert.Error(t, err)
	assert.EqualError(t, err, fmt.Sprintf("invalid filetype detected with extension '%s'", ext))
}

func TestCountFileLines(t *testing.T) {
	// Create a tempfile
	tempfile, err := ioutil.TempFile("/tmp", "line-counter-testfile1")
	assert.NoError(t, err)
	defer os.Remove(tempfile.Name())

	// Write data
	filedata := []byte("hello\ngo\nlast line")
	_, err = tempfile.Write(filedata)
	assert.NoError(t, err)
	tempfile.Close()

	// Count lines
	counter := newMockLineCounter()
	numLines := countFileLines(tempfile.Name(), counter)

	// Check result
	assert.Equal(t, 3, numLines)
}
