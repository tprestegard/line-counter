/*
Package cmd is used for generating CLI commands for the line-counter module.

Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"os"
	"sort"
	"strings"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var cfgFile string

// Map between strings and FileTypes; used for handling user-provided filetype arguments from
// the command line
var fileTypeMapping map[string]FileType = map[string]FileType{
	"golang": GolangFileType,
	"python": PythonFileType,
}

// NewRootCmd returns a command instance that represents the base command, when called without
// any subcommands
func NewRootCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "line-counter",
		Short: "A brief description of your application",
		Long: `A longer description that spans multiple lines and likely contains
examples and usage of using your application. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
		RunE: func(cmd *cobra.Command, args []string) error {
			// args should all be filenames

			// Get filetype flag value
			fileTypeFlagValue, _ := cmd.Flags().GetString("filetype")
			var definedFileType FileType

			// If provided, get corresponding FileType
			if fileTypeFlagValue != "" {
				if val, ok := fileTypeMapping[fileTypeFlagValue]; ok {
					definedFileType = val
				} else {
					// Get list of keys for error message
					keys := make([]string, 0, len(fileTypeMapping))
					for k := range fileTypeMapping {
						keys = append(keys, k)
					}
					// Sort keys
					sort.Strings(keys)
					keyStr := strings.Join(keys, ", ")
					return fmt.Errorf("Invalid filetype '%s'. Must be one of: %s", fileTypeFlagValue, keyStr)
				}
			}

			// TODO: Get "count docstrings" flag for Python

			totalLines := 0
			for _, filename := range args {

				// If no defined file type, auto-detect it based on file extension
				var fileType FileType
				if definedFileType != FileType("") {
					fileType = definedFileType
				} else {
					var err error
					fileType, err = detectFileType(filename)
					if err != nil {
						return fmt.Errorf("%s", err)
					}
				}

				// Get LineCounter instance based on file type
				var lc LineCounter
				if fileType == GolangFileType {
					lc = NewGolangLineCounter()
				} else if fileType == PythonFileType {
					lc = NewPythonLineCounter(false)
				}

				// Count file lines
				lines := countFileLines(filename, lc)

				// Print result
				fmt.Fprintf(cmd.OutOrStdout(), "%s: %d lines of code\n", filename, lines)

				// Sum up cumulative total lines for all provided files
				totalLines += lines
			}

			// If more than one file, print a summary at the bottom
			if len(args) > 1 {
				fmt.Fprintf(cmd.OutOrStdout(), strings.Repeat("-", 20)+"\n")
				fmt.Fprintf(cmd.OutOrStdout(), "TOTAL: %d\n", totalLines)
			}
			return nil
		},
	}

	// Define flags for command
	cmd.Flags().StringP("filetype", "f", "", "Help message for toggle")

	return cmd
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := NewRootCmd().Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.
	//rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.line-counter.yaml)")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	//rootCmd.Flags().StringP("filetype", "f", "", "Help message for toggle")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		// Search config in home directory with name ".line-counter" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".line-counter")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}
