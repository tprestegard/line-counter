package cmd

import (
	"regexp"
	"strings"
)

// LineCounter provides a CountLine function which takes a line string as input and returns a bool
type LineCounter interface {
	CountLine(line string) bool
}

// PythonLineCounter implements the LineCounter interface for Python files
type PythonLineCounter struct {
	countDocstrings                   bool
	docstringActive                   bool
	activeDocstringType               DocstringType
	previousLineClassOrFuncDefinition bool
}

// NewPythonLineCounter returns a PythonLineCounter interface. It takes one parameter as input,
// which determines whether docstrings are counted or not.
func NewPythonLineCounter(countDocstrings bool) *PythonLineCounter {
	return &PythonLineCounter{
		countDocstrings:                   countDocstrings,
		docstringActive:                   false,
		previousLineClassOrFuncDefinition: false,
	}
}

// lineIsDocstringStart checks whether a line is the start of a docstring
func (p *PythonLineCounter) lineIsDocstringStart(line string) bool {
	if !p.docstringActive && (strings.HasPrefix(line, string(DocstringTypeSingleQuote)) || strings.HasPrefix(line, string(DocstringTypeDoubleQuote))) && p.previousLineClassOrFuncDefinition {
		return true
	}
	return false
}

// lineIsDocstringEnd checks whether a line corresponds to the end of a docstring
func (p *PythonLineCounter) lineIsDocstringEnd(line string) bool {
	if p.docstringActive && strings.HasSuffix(line, string(p.activeDocstringType)) {
		return true
	}
	return false
}

// lineIsSingleLineDocstring checks whether a line corresponds to a single-line docstring
func (p *PythonLineCounter) lineIsSingleLineDocstring(line string) bool {
	if !p.docstringActive {
		if strings.HasPrefix(line, string(DocstringTypeSingleQuote)) && strings.HasSuffix(line, string(DocstringTypeSingleQuote)) && line != string(DocstringTypeSingleQuote) && p.previousLineClassOrFuncDefinition {
			return true
		}
		if strings.HasPrefix(line, string(DocstringTypeDoubleQuote)) && strings.HasSuffix(line, string(DocstringTypeDoubleQuote)) && line != string(DocstringTypeDoubleQuote) && p.previousLineClassOrFuncDefinition {
			return true
		}
	}
	return false
}

// CountLine determines whether a line in a Python file should be counted as code
func (p *PythonLineCounter) CountLine(line string) bool {
	line = strings.TrimSpace(line)

	rv := false

	// If we don't want to count docstrings, check if we are in a docstring
	// NOTE: at present, if we count docstrings, blank lines within docstrings
	// are still skipped
	if !p.countDocstrings {
		if p.lineIsSingleLineDocstring(line) {
			//fmt.Println("    Single line docstring found: " + line)
			goto End
		}
		if p.lineIsDocstringStart(line) {
			//fmt.Println("    Docstring start found: " + line)
			p.docstringActive = true
			if strings.HasPrefix(line, string(DocstringTypeDoubleQuote)) {
				p.activeDocstringType = DocstringTypeDoubleQuote
			} else {
				p.activeDocstringType = DocstringTypeSingleQuote
			}
			goto End
		}
		if p.lineIsDocstringEnd(line) {
			//fmt.Println("    Docstring end found: " + line)
			p.docstringActive = false
			goto End
		}
		if p.docstringActive {
			//fmt.Println("    Docstring active: " + line)
			goto End
		}
	}

	// Skip comments
	if strings.HasPrefix(line, "#") {
		//fmt.Println("    comment found")
		goto End
	}

	// Skip blank lines
	if line == "" {
		//fmt.Println("    blank line found")
		goto End
	}

	// Otherwise, count the line
	rv = true

End:
	if strings.HasPrefix(line, "def ") || strings.HasPrefix(line, "class ") && !p.docstringActive {
		p.previousLineClassOrFuncDefinition = true
	} else {
		p.previousLineClassOrFuncDefinition = false
	}
	return rv
}

// GolangLineCounter implements the LineCounter interface for Golang files
type GolangLineCounter struct {
	multilineCommentActive bool
}

// NewGolangLineCounter returns a GolangLineCounter instance
func NewGolangLineCounter() *GolangLineCounter {
	return &GolangLineCounter{}
}

// CountLine determines whether a line in a Golang file should be counted as code
func (g *GolangLineCounter) CountLine(line string) bool {
	// Remove everything between /* and */. We don't do this for cases where
	// multiline comment is active because we could have something like
	// "/* commentstart\n/* random */", and we would miss the end comment marker
	// in that case
	if !g.multilineCommentActive {
		re := regexp.MustCompile(`/\*.*?\*/`)
		line = re.ReplaceAllString(line, "")
	}

	// Trim spaces
	line = strings.TrimSpace(line)

	// Single line comments - remove everything after a //
	commentIndex := strings.Index(line, "//")
	if commentIndex != -1 {
		line = line[:commentIndex]
	}

	// Blank lines
	if line == "" {
		return false
	}

	// Multi-line comments
	if g.multilineCommentActive {
		endCommentIndex := strings.Index(line, "*/")
		if endCommentIndex == -1 {
			// No */ found - whole line is a comment
			return false
		}
		// Found a */ - remove everything up to and including that
		// substring, turn off multiline comment, then recur
		g.multilineCommentActive = false
		return g.CountLine(line[endCommentIndex+2:])
	}
	// We have already removed all paired /* */ above, so if there are
	// any /* left, it means we are starting a multiline comment
	if strings.Contains(line, "/*") {
		g.multilineCommentActive = true
	}

	// If the line starts with /*, whole line is a comment and shouldn't
	// be counted
	if strings.HasPrefix(line, "/*") {
		return false
	}

	// If we made it here, then there must be some real code on this line
	return true
}
