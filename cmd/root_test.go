package cmd

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

var pythonFileContent = `import os

# comment
def func1(a, b):
	return a*b
`

var golangFileContent = `import (
	"strings"
	"testing"
)

// comment
func Func1(a int, b int) int {
	return a*b
}
`

func TestExecuteRootCmdAutodetectFileType(t *testing.T) {
	cases := []struct {
		fileContent   string
		filename      string
		expectedLines int
	}{
		{
			fileContent:   pythonFileContent,
			filename:      "file-*.py",
			expectedLines: 3,
		},
		{
			fileContent:   golangFileContent,
			filename:      "file-*.go",
			expectedLines: 7,
		},
	}

	for _, c := range cases {
		// Open tempfile and add contents
		tempfile, _ := ioutil.TempFile("/tmp", c.filename)
		_, _ = tempfile.Write([]byte(c.fileContent))
		tempfile.Close()

		// Set args and call command
		b := bytes.NewBufferString("")
		cmd := NewRootCmd()
		cmd.SetOut(b)
		cmd.SetArgs([]string{tempfile.Name()})
		cmd.Execute()

		// Delete tempfile
		os.Remove(tempfile.Name())

		// Check output
		expectedOutput := fmt.Sprintf("%s: %d lines of code", tempfile.Name(), c.expectedLines)
		assert.Equal(t, expectedOutput, strings.TrimSpace(b.String()))
	}
}

func TestExecuteRootCmdAutodetectBadFileType(t *testing.T) {
	ext := ".txt"

	// Set args and call command
	b := bytes.NewBufferString("")
	cmd := NewRootCmd()
	cmd.SetOut(b)
	cmd.SetArgs([]string{"fake" + ext})
	cmd.Execute()

	// Get just first line of output, the rest is just the usage
	lines := strings.Split(b.String(), "\n")

	// Check output
	expectedOutput := fmt.Sprintf("Error: invalid filetype detected with extension '%s'", ext)
	assert.Equal(t, expectedOutput, lines[0])
}

func TestExecuteRootCmdProvidedFileType(t *testing.T) {
	cases := []struct {
		fileContent      string
		providedFileType string
		expectedLines    int
	}{
		{
			fileContent:      pythonFileContent,
			providedFileType: "python",
			expectedLines:    3,
		},
		{
			fileContent:      pythonFileContent,
			providedFileType: "golang",
			expectedLines:    4,
		},
		{
			fileContent:      golangFileContent,
			providedFileType: "python",
			expectedLines:    8,
		},
		{
			fileContent:      golangFileContent,
			providedFileType: "golang",
			expectedLines:    7,
		},
	}

	for _, c := range cases {
		// Open tempfile and add contents
		tempfile, _ := ioutil.TempFile("/tmp", "")
		_, _ = tempfile.Write([]byte(c.fileContent))
		tempfile.Close()

		// Set args and call command
		b := bytes.NewBufferString("")
		cmd := NewRootCmd()
		cmd.SetOut(b)
		cmd.SetArgs([]string{
			tempfile.Name(),
			fmt.Sprintf("--filetype=%s", c.providedFileType),
		})
		cmd.Execute()

		// Delete tempfile
		os.Remove(tempfile.Name())

		// Check output
		expectedOutput := fmt.Sprintf("%s: %d lines of code", tempfile.Name(), c.expectedLines)
		assert.Equal(t, expectedOutput, strings.TrimSpace(b.String()))
	}
}

func TestExecuteRootCmdBadProvidedFileType(t *testing.T) {
	providedFileType := "fake"

	// Set args and call command
	b := bytes.NewBufferString("")
	cmd := NewRootCmd()
	cmd.SetOut(b)
	cmd.SetArgs([]string{
		"fake.txt",
		fmt.Sprintf("--filetype=%s", providedFileType),
	})
	cmd.Execute()

	// Get just first line of output, the rest is just the usage
	lines := strings.Split(b.String(), "\n")

	// Check output
	expectedOutput := fmt.Sprintf("Error: Invalid filetype '%s'. Must be one of: golang, python", providedFileType)
	assert.Equal(t, expectedOutput, lines[0])
}

func TestMultipleFiles(t *testing.T) {
	// Create tempfiles
	tempfile1, _ := ioutil.TempFile("/tmp", "file-*.py")
	_, _ = tempfile1.Write([]byte(pythonFileContent))
	tempfile1.Close()
	tempfile2, _ := ioutil.TempFile("/tmp", "file-*.go")
	_, _ = tempfile2.Write([]byte(golangFileContent))
	tempfile2.Close()

	// Set args and call command
	b := bytes.NewBufferString("")
	cmd := NewRootCmd()
	cmd.SetOut(b)
	cmd.SetArgs([]string{tempfile1.Name(), tempfile2.Name()})
	cmd.Execute()

	// Clean up tempfiles
	os.Remove(tempfile1.Name())
	os.Remove(tempfile2.Name())

	// Check output
	expectedOutput := strings.Join(
		[]string{
			fmt.Sprintf("%s: %d lines of code", tempfile1.Name(), 3),
			fmt.Sprintf("%s: %d lines of code", tempfile2.Name(), 7),
			strings.Repeat("-", 20),
			fmt.Sprintf("TOTAL: %d", 10),
		},
		"\n",
	)
	assert.Equal(t, expectedOutput, strings.TrimSpace(b.String()))
}
