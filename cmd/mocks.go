package cmd

// mockLineCounter provides a mock implementation of LineCounter for testing purposes
type mockLineCounter struct{}

// CountLine just returns true for all inputs
func (m *mockLineCounter) CountLine(line string) bool {
	return true
}

// newMockLineCounter returns a mockLineCounter instance
func newMockLineCounter() *mockLineCounter {
	return &mockLineCounter{}
}
