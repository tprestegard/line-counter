package cmd

// DocstringType is a custom type for Python docstrings
type DocstringType string

// Docstring type constants
const (
	DocstringTypeDoubleQuote DocstringType = "\"\"\""
	DocstringTypeSingleQuote DocstringType = "'''"
)

// FileType is a custom type for code-specific files
type FileType string

// File type constants
const (
	GolangFileType FileType = "golang"
	PythonFileType FileType = "python"
)
