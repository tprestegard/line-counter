package cmd

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

var pythonCode1 string = `import os

def test_func()
	"""
	my docstring
	"""
	return 1 + 3

class MyClass:
	'''One-line docstring   '''

test_with_triple_quotes = """
a string
'''
"""

class MyClass2:
	"""docstring
	'''

	"""
	def method(self):
		"""
		test func definition in docstring
		def example():
			return 2
		"""
		return 9

y = 3
`

var golangCode1 string = `package test

import (
	"strings"
	"testing"
)

func TestFunc() bool {
	// this is a comment
	x := 1

	y := 2

	/* Test a single line comment */

	/* Here is a
	multi-line comment

	xyz /* nested comment
	// another comment
	z := 23
	a := 12 + 3*/

	/* multiline comment
	comment */ m := 2

	/* two comments */ ab := 3 /* on one line */

	/* last test
	ok */ m := 4 /* ok */

	return false
}
`

func TestPythonLineCounterBasicLine(t *testing.T) {
	cases := []struct {
		line              string
		expectedCountLine bool
	}{
		{line: "", expectedCountLine: false},
		{line: "      ", expectedCountLine: false},
		{line: "x = 3", expectedCountLine: true},
		{line: "class LineCounter(CounterBase):", expectedCountLine: true},
	}

	for _, c := range cases {
		// Setup
		line := strings.TrimSpace(c.line)
		counter := NewPythonLineCounter(false)

		countLine := counter.CountLine(line)
		assert.Equal(t, c.expectedCountLine, countLine)
	}
}

func TestPythonLineCounterComment(t *testing.T) {
	cases := []struct {
		line              string
		expectedCountLine bool
	}{
		{line: "# comment", expectedCountLine: false},
		{line: "      # comment", expectedCountLine: false},
		{line: "x = 3 # post-code comment", expectedCountLine: true},
	}

	for _, c := range cases {
		// Setup
		line := strings.TrimSpace(c.line)
		counter := NewPythonLineCounter(false)

		countLine := counter.CountLine(line)
		assert.Equal(t, c.expectedCountLine, countLine)
	}
}

func TestPythonLineCounterClassOrFuncDefinition(t *testing.T) {
	cases := []struct {
		line                                      string
		expectedPreviousLineClassOrFuncDefinition bool
	}{
		{line: "# comment", expectedPreviousLineClassOrFuncDefinition: false},
		{line: "def func(a, b, c):", expectedPreviousLineClassOrFuncDefinition: true},
		{line: "class TestClass(abc.ABC):", expectedPreviousLineClassOrFuncDefinition: true},
		{line: "    def func(a, b, c):", expectedPreviousLineClassOrFuncDefinition: true},
		{line: "    class TestClass(abc.ABC):", expectedPreviousLineClassOrFuncDefinition: true},
	}

	for _, c := range cases {
		// Setup
		line := strings.TrimSpace(c.line)
		counter := NewPythonLineCounter(false)

		_ = counter.CountLine(line)
		assert.Equal(t, c.expectedPreviousLineClassOrFuncDefinition, counter.previousLineClassOrFuncDefinition)
	}
}

func TestPythonLineCounterClassOrFuncDefinitionReset(t *testing.T) {
	cases := []struct {
		line                                      string
		expectedPreviousLineClassOrFuncDefinition bool
	}{
		{line: "", expectedPreviousLineClassOrFuncDefinition: false},
		{line: "# comment", expectedPreviousLineClassOrFuncDefinition: false},
		{line: "def func(a, b, c):", expectedPreviousLineClassOrFuncDefinition: true},
		{line: "class TestClass(abc.ABC):", expectedPreviousLineClassOrFuncDefinition: true},
	}

	for _, c := range cases {
		// Setup
		line := strings.TrimSpace(c.line)
		counter := NewPythonLineCounter(false)
		counter.previousLineClassOrFuncDefinition = true

		_ = counter.CountLine(line)
		assert.Equal(t, c.expectedPreviousLineClassOrFuncDefinition, counter.previousLineClassOrFuncDefinition)
	}
}

func TestPythonLineCounterSingleLineDocstring(t *testing.T) {
	cases := []struct {
		line              string
		expectedCountLine bool
	}{
		{line: "\"\"\"test\"\"\"", expectedCountLine: false},
		{line: "\"\"\"test'''\"\"\"", expectedCountLine: false},
		{line: "''' test '''", expectedCountLine: false},
	}

	for _, c := range cases {
		// Setup
		line := strings.TrimSpace(c.line)
		counter := NewPythonLineCounter(false)
		counter.previousLineClassOrFuncDefinition = true

		countLine := counter.CountLine(line)
		assert.Equal(t, c.expectedCountLine, countLine)
		assert.Equal(t, counter.docstringActive, false)
	}
}

func TestPythonLineCounterSingleLineDocstringCountDocstrings(t *testing.T) {
	cases := []struct {
		line string
	}{
		{line: "\"\"\"test\"\"\""},
		{line: "\"\"\"test'''\"\"\""},
		{line: "''' test '''"},
	}

	for _, c := range cases {
		// Setup
		line := strings.TrimSpace(c.line)
		counter := NewPythonLineCounter(true)
		counter.previousLineClassOrFuncDefinition = true

		countLine := counter.CountLine(line)
		assert.Equal(t, true, countLine)
		assert.Equal(t, false, counter.docstringActive)
	}
}

func TestPythonLineCounterDocstringStart(t *testing.T) {
	cases := []struct {
		line                        string
		expectedActiveDocstringType DocstringType
	}{
		{line: "\"\"\"", expectedActiveDocstringType: DocstringTypeDoubleQuote},
		{line: "\"\"\"test", expectedActiveDocstringType: DocstringTypeDoubleQuote},
		{line: "'''", expectedActiveDocstringType: DocstringTypeSingleQuote},
		{line: "'''test", expectedActiveDocstringType: DocstringTypeSingleQuote},
		{line: "'''test\"\"\"", expectedActiveDocstringType: DocstringTypeSingleQuote},
	}

	for _, c := range cases {
		// Setup
		line := strings.TrimSpace(c.line)
		counter := NewPythonLineCounter(false)
		counter.previousLineClassOrFuncDefinition = true

		countLine := counter.CountLine(line)
		assert.Equal(t, false, countLine)
		assert.Equal(t, true, counter.docstringActive)
		assert.Equal(t, c.expectedActiveDocstringType, counter.activeDocstringType)
	}
}

func TestPythonLineCounterDocstringEnd(t *testing.T) {
	cases := []struct {
		line                string
		activeDocstringType DocstringType
	}{
		{line: "\"\"\"", activeDocstringType: DocstringTypeDoubleQuote},
		{line: "test\"\"\"", activeDocstringType: DocstringTypeDoubleQuote},
		{line: "'''", activeDocstringType: DocstringTypeSingleQuote},
		{line: "test'''", activeDocstringType: DocstringTypeSingleQuote},
		{line: "'''test\"\"\"", activeDocstringType: DocstringTypeDoubleQuote},
	}

	for _, c := range cases {
		// Setup
		line := strings.TrimSpace(c.line)
		counter := NewPythonLineCounter(false)
		counter.activeDocstringType = c.activeDocstringType
		counter.docstringActive = true

		countLine := counter.CountLine(line)
		assert.Equal(t, false, countLine)
		assert.Equal(t, false, counter.docstringActive)
	}
}

func TestPythonLineCounterDocstringCountDocstrings(t *testing.T) {
	cases := []struct {
		line string
	}{
		{line: "\"\"\""},
		{line: "test\"\"\""},
		{line: "'''"},
		{line: "test'''"},
		{line: "'''test\"\"\""},
		{line: "'''test'''"},
		{line: "\"\"\"   test '''\"\"\""},
	}

	for _, c := range cases {
		// Setup
		line := strings.TrimSpace(c.line)
		counter := NewPythonLineCounter(true)
		counter.previousLineClassOrFuncDefinition = true

		countLine := counter.CountLine(line)
		assert.Equal(t, true, countLine)
		assert.Equal(t, false, counter.docstringActive)
		assert.Equal(t, DocstringType(""), counter.activeDocstringType)
	}
}

func TestCountLinesPythonLineCounter(t *testing.T) {
	cases := []struct {
		countDocstrings   bool
		expectedLineCount int
	}{
		{
			countDocstrings:   true,
			expectedLineCount: 24,
		},
		{
			countDocstrings:   false,
			expectedLineCount: 12,
		},
	}

	lines := strings.Split(pythonCode1, "\n")
	for _, c := range cases {
		lineCount := 0
		counter := NewPythonLineCounter(c.countDocstrings)
		for _, line := range lines {
			line = strings.TrimSpace(line)
			countLine := counter.CountLine(line)
			if countLine {
				//fmt.Println(line)
				lineCount++
			}
		}
		assert.Equal(t, c.expectedLineCount, lineCount)
	}
}

func TestGolangLineCounterBasicLine(t *testing.T) {
	cases := []struct {
		line              string
		expectedCountLine bool
	}{
		{line: "", expectedCountLine: false},
		{line: "      ", expectedCountLine: false},
		{line: "x := 3", expectedCountLine: true},
		{line: "func newFunc(test string) (bool, err) {", expectedCountLine: true},
	}

	for _, c := range cases {
		// Setup
		line := strings.TrimSpace(c.line)
		counter := NewGolangLineCounter()

		countLine := counter.CountLine(line)
		assert.Equal(t, c.expectedCountLine, countLine)
	}
}

func TestGolangLineCounterComment(t *testing.T) {
	cases := []struct {
		line              string
		expectedCountLine bool
	}{
		{line: "// comment", expectedCountLine: false},
		{line: "/* comment */", expectedCountLine: false},
		{line: "      // comment", expectedCountLine: false},
		{line: "      /* comment */", expectedCountLine: false},
		{line: "x = 3 // post-code comment", expectedCountLine: true},
		{line: "x = 3 /* post-code comment */", expectedCountLine: true},
	}

	for _, c := range cases {
		// Setup
		line := strings.TrimSpace(c.line)
		counter := NewGolangLineCounter()

		countLine := counter.CountLine(line)
		assert.Equal(t, c.expectedCountLine, countLine)
	}
}

func TestGolangLineCounterMultilineCommentStart(t *testing.T) {
	cases := []struct {
		line              string
		expectedCountLine bool
	}{
		{line: "/* blah", expectedCountLine: false},
		{line: "x := 3 /* blah", expectedCountLine: true},
	}

	for _, c := range cases {
		// Setup
		line := strings.TrimSpace(c.line)
		counter := NewGolangLineCounter()

		countLine := counter.CountLine(line)
		assert.Equal(t, c.expectedCountLine, countLine)
		assert.Equal(t, true, counter.multilineCommentActive)
	}
}

func TestGolangLineCounterComplexMultilineComment(t *testing.T) {
	cases := []struct {
		line                           string
		initialMultilineCommentActive  bool
		expectedCountLine              bool
		expectedMultilineCommentActive bool
	}{
		{
			line:                           "/* blah",
			initialMultilineCommentActive:  false,
			expectedCountLine:              false,
			expectedMultilineCommentActive: true,
		},
		{
			line:                           "/* blah */",
			initialMultilineCommentActive:  true,
			expectedCountLine:              false,
			expectedMultilineCommentActive: false,
		},
		{
			line:                           "/* blah */",
			initialMultilineCommentActive:  false,
			expectedCountLine:              false,
			expectedMultilineCommentActive: false,
		},
		{
			line:                           "comment */ x := 3 /* new",
			initialMultilineCommentActive:  true,
			expectedCountLine:              true,
			expectedMultilineCommentActive: true,
		},
		{
			line:                           "/* comment; x := 3 */",
			initialMultilineCommentActive:  true,
			expectedCountLine:              false,
			expectedMultilineCommentActive: false,
		},
		{
			line:                           "/* comment */ x := 9",
			initialMultilineCommentActive:  false,
			expectedCountLine:              true,
			expectedMultilineCommentActive: false,
		},
		{
			line:                           "ok */ x := 9 /* yep */ //ok /*",
			initialMultilineCommentActive:  true,
			expectedCountLine:              true,
			expectedMultilineCommentActive: false,
		},
	}

	for _, c := range cases {
		// Setup
		line := strings.TrimSpace(c.line)
		counter := NewGolangLineCounter()
		counter.multilineCommentActive = c.initialMultilineCommentActive

		countLine := counter.CountLine(line)
		assert.Equal(t, c.expectedCountLine, countLine)
		assert.Equal(t, c.expectedMultilineCommentActive, counter.multilineCommentActive, line)
	}
}

func TestCountLinesGolangLineCounter(t *testing.T) {
	lines := strings.Split(golangCode1, "\n")
	lineCount := 0
	counter := NewGolangLineCounter()
	for _, line := range lines {
		line = strings.TrimSpace(line)
		countLine := counter.CountLine(line)
		if countLine {
			//fmt.Println(line)
			lineCount++
		}
	}
	assert.Equal(t, 13, lineCount)
}
